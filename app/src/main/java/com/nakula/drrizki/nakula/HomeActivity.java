package com.nakula.drrizki.nakula;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    private String[] imageUrls = new String[]{
            "https://global.yamaha-motor.com/business/img/pic_001.jpg",
            "http://1.bp.blogspot.com/-mgFkNn4ytpY/Ub1g1VlBMHI/AAAAAAAAAPA/u0BJ_VQFQ0s/s1600/Motor+Yamaha+2014.jpg",
            "https://global.yamaha-motor.com/showroom/event/tokyo-motorshow-2017/assets/img/exhibitionmodels/motoroid/mainvisual.jpg",
            "https://www.otomotifo.com/wp-content/uploads/2017/10/Motor-Yamaha-Terbaik.jpg",
            "http://semarmotoblog.com/wp-content/uploads/2018/07/warna-yamaha-grand-filano-hybrid-2018-hitam-black.png"
    };

    CardView gaji, absen, pengobatan, kredit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //berita dengan image slider manual
        ViewPager viewPager = findViewById(R.id.view_pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(this, imageUrls);
        viewPager.setAdapter(adapter);

        //deklarasi
        gaji = (CardView) findViewById(R.id.gaji);
        absen = (CardView) findViewById(R.id.absen);
        pengobatan = (CardView) findViewById(R.id.pengobatan);
        kredit = (CardView) findViewById(R.id.bpkb);

        //pindah layout1
        gaji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, GajiActivity.class);
                startActivity(i);
            }
        });
        absen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, AbsenActivity.class);
                startActivity(i);
            }
        });
        //pindah layout5
        pengobatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, PengobatanActivity.class);
                startActivity(i);
            }
        });
        //pindah layout6
        kredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, KreditActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu); //your file name
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                Intent profile = new Intent(this, ProfilActivity.class);
                startActivity(profile);
                return true;
            case R.id.pengaturan:
                Intent pengaturan = new Intent(this, PengaturanActivity.class);
                startActivity(pengaturan);
                return true;
            case R.id.logout:
                Intent logout = new Intent(this, MainActivity.class);
                startActivity(logout);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
