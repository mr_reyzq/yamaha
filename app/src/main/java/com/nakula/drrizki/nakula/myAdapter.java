package com.nakula.drrizki.nakula;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class myAdapter extends RecyclerView.Adapter<myAdapter.ViewHolder> {

    private Context context;
    private List<KumpulanData> data;

    public myAdapter(Context context, List<KumpulanData> data) {
        this.context = context;
        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView no, tanggal,head, isi, head2, isi2, head3, isi3, head4, isi4;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            no = (TextView) itemView.findViewById(R.id.no);
            tanggal = (TextView) itemView.findViewById(R.id.tanggal);
            head = (TextView) itemView.findViewById(R.id.head);
            isi = (TextView) itemView.findViewById(R.id.isi);
            head2 = (TextView) itemView.findViewById(R.id.head2);
            isi2 = (TextView) itemView.findViewById(R.id.isi2);
            head3 = (TextView) itemView.findViewById(R.id.head3);
            isi3 = (TextView) itemView.findViewById(R.id.isi3);
            head4 = (TextView) itemView.findViewById(R.id.head4);
            isi4 = (TextView) itemView.findViewById(R.id.isi4);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.recycler_view_gaji, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        KumpulanData pu = data.get(position);
        holder.no.setText(pu.getNo());
        holder.tanggal.setText(pu.getTanggal());
        holder.head.setText(pu.getNama());
        holder.isi.setText(pu.getIsi());
        holder.head2.setText(pu.getNama2());
        holder.isi2.setText(pu.getIsi2());
        holder.head3.setText(pu.getNama3());
        holder.isi3.setText(pu.getIsi3());
        holder.head4.setText(pu.getNama4());
        holder.isi4.setText(pu.getIsi4());

        if (position%2 == 0) {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.merahDop));
        }
        else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.putih));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
