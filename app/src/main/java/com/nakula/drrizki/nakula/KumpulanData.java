package com.nakula.drrizki.nakula;

import android.view.View;

public class KumpulanData {

    String no, tanggal, nama, isi, nama2, isi2, nama3, isi3, nama4, isi4;

    public KumpulanData(String no, String tanggal, String nama, String isi, String nama2, String isi2, String nama3, String isi3, String nama4, String isi4) {
        this.no = no;
        this.tanggal = tanggal;
        this.nama = nama;
        this.isi = isi;
        this.nama2 = nama2;
        this.isi2 = isi2;
        this.nama3 = nama3;
        this.isi3 = isi3;
        this.nama4 = nama4;
        this.isi4 = isi4;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getNama2() {
        return nama2;
    }

    public void setNama2(String nama2) {
        this.nama2 = nama2;
    }

    public String getIsi2() {
        return isi2;
    }

    public void setIsi2(String isi2) {
        this.isi2 = isi2;
    }

    public String getNama3() {
        return nama3;
    }

    public void setNama3(String nama3) {
        this.nama3 = nama3;
    }

    public String getIsi3() {
        return isi3;
    }

    public void setIsi3(String isi3) {
        this.isi3 = isi3;
    }

    public String getNama4() {
        return nama4;
    }

    public void setNama4(String nama4) {
        this.nama4 = nama4;
    }

    public String getIsi4() {
        return isi4;
    }

    public void setIsi4(String isi4) {
        this.isi4 = isi4;
    }


}
