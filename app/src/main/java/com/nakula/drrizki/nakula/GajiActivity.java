package com.nakula.drrizki.nakula;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class GajiActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;

    List<KumpulanData> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gaji);

        recyclerView = (RecyclerView) findViewById(R.id.recycle_gaji);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        data = new ArrayList<KumpulanData>();

        data.add(new KumpulanData("1", "10/05/2018", "Rp. ", "5.000.000", "Rp. ","1.000.000",
                "Rp. ","2.000.000","Rp. ","6.000.000"));
        data.add(new KumpulanData("2", "30/05/2018", "Rp. ", "4.000.000", "Rp. ","2.000.000",
                "Rp. ","2.000.000","Rp. ","4.000.000"));
        data.add(new KumpulanData("3", "30/05/2018", "Rp. ", "4.000.000", "Rp. ","2.000.000",
                "Rp. ","2.000.000","Rp. ","4.000.000"));
        data.add(new KumpulanData("4", "30/05/2018", "Rp. ", "4.000.000", "Rp. ","2.000.000",
                "Rp. ","2.000.000","Rp. ","4.000.000"));
        data.add(new KumpulanData("5", "30/05/2018", "Rp. ", "4.000.000", "Rp. ","2.000.000",
                "Rp. ","2.000.000","Rp. ","4.000.000"));
        data.add(new KumpulanData("6", "30/05/2018", "Rp. ", "4.000.000", "Rp. ","2.000.000",
                "Rp. ","2.000.000","Rp. ","4.000.000"));

        mAdapter = new myAdapter(this, data);

        recyclerView.setAdapter(mAdapter);

    }
}
